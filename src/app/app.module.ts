import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AlbumsComponent } from './albums/albums.component';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AlbumDetailComponent } from './albums/album-detail/album-detail.component';
import { AlbumService } from './albums/album.service';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import {NgxPaginationModule} from 'ngx-pagination';
import { ImageGridComponent } from './albums/image-grid/image-grid.component';

const appRoutes: Routes = [
  {
    path: 'albums',
    component: AlbumsComponent,
    data: { title: 'Albums' }
  },
  {
    path: 'album-detail/:id',
    component: AlbumDetailComponent,
    data: { title: 'Album Details' }
  },
  { path: '',
    redirectTo: '/albums',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    AlbumsComponent,
    AlbumDetailComponent,
    HeaderComponent,
    SidebarComponent,
    ImageGridComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxPaginationModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    AlbumService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
