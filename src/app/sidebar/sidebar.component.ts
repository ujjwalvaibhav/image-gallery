import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../albums/album.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  constructor(private service: AlbumService) { }

  ngOnInit() {
  }

  getRecords(numOfRecords: number) {
    this.service.setRecords(numOfRecords);
  }

}
