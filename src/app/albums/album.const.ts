export const CONST = {
    start: 0,
    limit: 20,
    userImages: [
        {
            id: 1,
            src: 'https://via.placeholder.com/150/5e5e5e'
        },
        {
            id: 2,
            src: 'https://via.placeholder.com/150/00ff'
        },
        {
            id: 3,
            src: 'https://via.placeholder.com/150/5a0000'
        },
        {
            id: 4,
            src: 'https://via.placeholder.com/150/5a00fe'
        },
        {
            id: 5,
            src: 'https://via.placeholder.com/150/7e6f6f'
        },
        {
            id: 6,
            src: 'https://via.placeholder.com/150/124823'
        },
        {
            id: 7,
            src: 'https://via.placeholder.com/150/2f5359'
        },
        {
            id: 8,
            src: 'https://via.placeholder.com/150/a78b28'
        },
        {
            id: 9,
            src: 'https://via.placeholder.com/150/070604'
        },
        {
            id: 10,
            src: 'https://via.placeholder.com/150/f3a509'
        }
    ]
};

