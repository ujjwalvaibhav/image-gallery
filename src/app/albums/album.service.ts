import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class AlbumService {
    constructor(private http: HttpClient) { }

 numOfRecords = new Subject<number>();
 recordsEmitter = this.numOfRecords.asObservable();

 /**
  * This method is calling api to get albums.
  * It has two input params
  * @param start (type: number) start point of data, array index
  * @param limit (type: number) Number of records you want to fetch
  */
 getAlbums(start: number, limit: number) {
    return this.http.get(
      `https://jsonplaceholder.typicode.com/albums?_start=${start}&_limit=${limit}`
    );
  }

  /**
   * This method used to get all photos
   * inside a album based on album id
   * @param albumId (type: number)
   * @param start (type: number) start point of data, array index
   * @param limit (type: number) Number of records you want to fetch
   */
  getPhotos(albumId: number, start: number, limit: number) {
    return this.http.get(
      `https://jsonplaceholder.typicode.com/photos?albumId=${albumId}&_start=${start}&_limit=${limit}`
    );
  }

  /**
   * This is a observable method is used to set the number
   * of records to show per page. It will semd the number to component
   * @param numOfRecords How many records you want to
   * see on single page (20, 30, 50)
   */
  setRecords(numOfRecords: number) {
    this.numOfRecords.next(numOfRecords);
  }

}
