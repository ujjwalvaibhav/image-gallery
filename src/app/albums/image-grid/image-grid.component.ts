declare let $: any;

import { Component, OnInit, Input } from '@angular/core';
import { AlbumService } from '../album.service';
import { CONST } from '../album.const';
import { PaginationInstance } from 'ngx-pagination';
import { Router } from '@angular/router';
import * as $ from 'jquery';
import * as TYPE from '../album.type';

@Component({
  selector: 'app-image-grid',
  templateUrl: './image-grid.component.html',
  styleUrls: ['./image-grid.component.css']
})
export class ImageGridComponent implements OnInit {

  @Input() records: Array<object>;
  @Input() type: string;

  currentAlbum: TYPE.AlbumsModel;
  photoData: TYPE.PhotosModel;
  pageTitle = 'Our Albums';

  // This is the  configuration of pagination
  public filter = '';
  public maxSize = 7;
  public directionLinks = true;
  public autoHide = false;
  public responsive = true;
  public config: PaginationInstance = {
    id: 'photos',
    itemsPerPage: CONST.limit,
    currentPage: 1
  };
  public labels: any = {
    previousLabel: 'Previous',
    nextLabel: 'Next',
    screenReaderPaginationLabel: 'Pagination',
    screenReaderPageLabel: 'page',
    screenReaderCurrentLabel: `You're on page`
  };
  // Paginate config ends here

  constructor(
    private service: AlbumService,
    private router: Router
    ) { }

  ngOnInit() {
    this.updatePerPageItems();
    if (this.type === 'photos') {
      this.currentAlbum = JSON.parse(sessionStorage.getItem('currentAlbum'));
      this.pageTitle = this.currentAlbum.title + ' by ' + this.currentAlbum.userId;
    }
  }

  /**
   * This is a watcher which will keep looking if user
   * is modifying the show per page filter from sidebar
   * and update the records and pagination
   */
  updatePerPageItems() {
    this.service.recordsEmitter.subscribe(res => {
      this.config.itemsPerPage = res;
    });
  }

  /**
   * This method used to get the different
   * images based on userid or without it
   * @param item (type: object)
   */
  getUserImage(item) {
    let imageSource = '';
    if (this.type === 'photos') {
      imageSource = item.thumbnailUrl;
    } else {
      const user = CONST.userImages.find(ele => ele.id === item.userId);
      imageSource = user.src;
    }
    return imageSource;
  }

  /**
   * This method will diplay details on
   * next page based on item selected
   * @param item (type: object)
   */
  getDetails(item: Object) {
    if (this.type === 'albums') {
      this.router.navigate(['album-detail/' + item['id']]);
      sessionStorage.setItem('currentAlbum', JSON.stringify(item));
    } else {
      this.showPhotoDetail(item);
    }
  }

  /**
   * This method will open a modal consisting
   * photo and its details
   * @param photoDetails (type: object) contains
   * all the data related to a photo
   */
  showPhotoDetail (photoDetails) {
    this.photoData = photoDetails;
    setTimeout(() => {
      $('#photoModal').modal('show');
    }, 100);
  }

  /**
   * This method will shorten the title if the
   * length exceeds to keep the design consistant
   * Full title can be seen as popover when hover over the title
   * @param title (type: string)
   */
  getTitle(title: string) {
    return title.length > 19 ? (title.slice(0, 16) + '...') : title;
  }

  /**
   * This method is called when we
   * change the page number in pagination
   * @param number (type: number)
   */
  onPageChange(number: number) {
    this.config.currentPage = number;
  }

}
