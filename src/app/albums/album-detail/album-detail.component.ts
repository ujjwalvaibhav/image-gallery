import { Component, OnInit } from '@angular/core';
import { AlbumService } from '../album.service';
import * as TYPE from '../album.type';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-album-detail',
  templateUrl: './album-detail.component.html',
  styleUrls: ['./album-detail.component.css']
})
export class AlbumDetailComponent implements OnInit {

  photos: any = []; // Array which will store all the albums
  showError = false; // This is used to show server errors
  albumId: number;

  constructor(
    private service: AlbumService,
    private router: Router,
    private route: ActivatedRoute
    ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.albumId = params.id;
    });
    this.getPhotos();
  }

  /**
   * This method is used to get all albums present in our records
   * will get all the albums data
   * Because we have limited number of records(100),
   * we can get all at once and can have client side pagination
   */
  getPhotos() {
    this.service.getPhotos(this.albumId, 0, 100)
      .subscribe((res: TYPE.PhotosModel) => {
        if (res) {
          this.photos = res;
        }
      }, () => {
        this.ShowServerError();
      });
  }

  /**
 * This will show a alert if somehow api fails to respond
 * The alert will be disappeared after 5 seconds
 * @output this.showError Boolean : To hide show the alert box in html
 */
  ShowServerError() {
    this.showError = true;
    setTimeout(() => {
      this.showError = false;
    }, 5000);
  }

}
