import { Component, OnInit } from '@angular/core';
import { AlbumService } from './album.service';
import * as TYPE from './album.type';

@Component({
  selector: 'app-albums',
  templateUrl: './albums.component.html',
  styleUrls: ['./albums.component.css']
})
export class AlbumsComponent implements OnInit {

  albums: any = []; // Array which will store all the albums
  showError = false; // This is used to show server errors

  constructor(private service: AlbumService) { }

  ngOnInit() {
    this.getAlbums();
  }

  /**
   * This method is used to get all albums present in our records
   * will get all the albums data
   * Because we have limited number of records(100),
   * we can get all at once and can have client side pagination
   */
  getAlbums() {
    this.service.getAlbums(0, 100)
      .subscribe((res: TYPE.AlbumsModel) => {
        if (res) {
          this.albums = res;
        }
      }, () => {
        this.ShowServerError();
      });
  }

  /**
 * This will show a alert if somehow api fails to respond
 * The alert will be disappeared after 5 seconds
 * @output this.showError Boolean : To hide show the alert box in html
 */
  ShowServerError() {
    this.showError = true;
    setTimeout(() => {
      this.showError = false;
    }, 5000);
  }

}
