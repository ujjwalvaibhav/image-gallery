export interface AlbumsModel {
    userId: number;
    id: number;
    title: String;
}

export interface PhotosModel {
    albumId: number;
    userId: number;
    id: number;
    title: String;
    url: string;
    thumbnailUrl: string;
}
